import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:app_trip/common/floating_action_button_green.dart';

class CardImagePlace extends StatelessWidget{

  String pathImage = "assets/img/peru.jpg";
  String name = "Knuckles Mountains Range";
  String ubication = "Hiking, Water fall hunting, Natural bath, Scenary and Photography";
  String steps = "Steps  123,123,123";

  CardImagePlace();
  @override
  Widget build(BuildContext context) {

    final cardImage = Container(
      margin: EdgeInsets.all(10.0),
      width: 340,
      height: 200,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(pathImage),
          fit: BoxFit.cover,
        ),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15.0),
      ),
    );

    final namePlace = Container(
      margin: EdgeInsets.only(top:10.0, bottom:5.0, left: 15.0),
      child: Text(
        name,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15.0,
          fontFamily:'Lato'
        ),
      ),
    );

    final ubicationPlace = Container(
      margin: EdgeInsets.only(top:8.0, bottom:5.0, left: 15.0, right: 15.0),
      child: Text(
        ubication,
        style: TextStyle(
          fontSize: 10.0,
          fontFamily: 'Lato',
          color: Colors.grey
        ),
      ),
    );

    final stepsDescription = Container(
      margin: EdgeInsets.only(top:8.0, bottom:5.0, left: 15.0),
      child: Text(
        steps,
        style: TextStyle(
            fontSize: 15.0,
            fontFamily: 'Lato',
            color: Colors.deepOrange
        ),
      ),
    );

    final  columnDescription = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        namePlace,
        ubicationPlace,
        stepsDescription,
      ],
    );

    final cardDescription = Center(
      child: Container(
        margin: EdgeInsets.only(top: 150.0),
        width: 280,
        height: 120,
        child: Card(
          child: columnDescription,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(5),
        ),
      ),
    );

    final cardButton = Center(
      child: Container(
        margin: EdgeInsets.only(top:230.0, left:180.0),
          child: FloatingActionButtonGreen()
      ),
    );

    return Container(
      child: Stack(
       children: [
         cardImage,
         cardDescription,
         cardButton,
       ],
      )
    );
  }
}