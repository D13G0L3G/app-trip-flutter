import 'package:flutter/material.dart';

import 'gradient_back_profile.dart';
import 'card_profile.dart';
import 'card_place_list.dart';

class ContentViewProfile extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GradientBackProfile("Profile"),
        CardProfile(),
        CardPlaceList(),
      ],
    );
  }
}