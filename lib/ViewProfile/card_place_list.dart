import 'package:flutter/material.dart';
import 'card_image_place.dart';

class CardPlaceList extends StatelessWidget{

  CardPlaceList();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top:250),
      child: ListView(
        scrollDirection: Axis.vertical,
        children: [
          CardImagePlace(),
          CardImagePlace(),
          CardImagePlace(),
          CardImagePlace(),
          CardImagePlace(),
        ],
      ),
    );
  }
}