import 'package:flutter/material.dart';

class NavFloatingButtonsProfile extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _NavFloatingButtonsProfile();
  }
}

class _NavFloatingButtonsProfile extends State<NavFloatingButtonsProfile>{

  void onPressedFav(){
    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text("Presionaste en un Floating Button del nav"))
    );
  }

  void onPressedBtnAdd(){
    Scaffold.of(context).showSnackBar(
        SnackBar(content: Text("OnPressed Button add"))
    );
  }

  @override
  Widget build(BuildContext context) {
    final buttonBookMark = FloatingActionButton(
      backgroundColor: Color(0xFFA7B2E3),
      foregroundColor: Color(0xFF584CD1),
      tooltip: "Fav",
      onPressed: onPressedFav,
      mini: true,
      child: Icon(Icons.bookmark_border),
    );

    final buttonTv = FloatingActionButton(
      backgroundColor: Color(0xFFA7B2E3),
      foregroundColor: Color(0xFF584CD1),
      tooltip: "Fav",
      onPressed: onPressedFav,
      mini: true,
      child: Icon(Icons.tv),
    );

    final buttonAdd = FloatingActionButton(
      backgroundColor: Color(0xFFFFFFFF),
      foregroundColor: Color(0xFF584CD1),
      tooltip: "Fav",
      onPressed: onPressedBtnAdd,
      mini: false,
      child: Icon(Icons.add),
    );

    final buttonEmail = FloatingActionButton(
      backgroundColor: Color(0xFFA7B2E3),
      foregroundColor: Color(0xFF584CD1),
      tooltip: "Fav",
      onPressed: onPressedFav,
      mini: true,
      child: Icon(Icons.email),
    );

    final buttonPerson = FloatingActionButton(
      backgroundColor: Color(0xFFA7B2E3),
      foregroundColor: Color(0xFF584CD1),
      tooltip: "Fav",
      onPressed: onPressedFav,
      mini: true,
      child: Icon(Icons.person),
    );

    return Container(
      margin: EdgeInsets.only(top:190.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          buttonBookMark,
          buttonTv,
          buttonAdd,
          buttonEmail,
          buttonPerson,
        ],
      ),
    );
  }
}