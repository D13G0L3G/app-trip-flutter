import 'package:flutter/material.dart';

class CardProfile extends StatelessWidget {

  String name = "Sofia Perez";
  String email = "womantravel@mail.com";
  String pathImage = "assets/img/mujer_4.jpg";

  CardProfile();
  @override
  Widget build(BuildContext context) {

    final imageProfile = Container(
      margin: EdgeInsets.only(left: 10.0),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage(pathImage),
          fit: BoxFit.fill,
        ),
        boxShadow: <BoxShadow> [
          BoxShadow(
            color: Color(0xFFFFFFFF),
            spreadRadius: 3,
          )
        ]
      ),
    );

    final userName = Container(
      margin: EdgeInsets.only(left: 10.0, top: 25.0),
      child: Text(
        name,
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'Lato',
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );

    final userEmail = Container(
      margin: EdgeInsets.only(left: 10.0, top: 5.0),
      child: Text(
        email,
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'Lato',
          fontSize: 15.0,
        ),
      ),
    );

    final dataProfile = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        userName,
        userEmail,
      ],
    );

    return  Container(
      margin: EdgeInsets.only(top:90.0, right: 25.0, left: 25.0),
      width: 300,
      height: 100,
      child:Row(
        children: [
          imageProfile,
          dataProfile,
        ],
      ),
    );
  }
}