import 'package:flutter/material.dart';

class GradientBackProfile extends StatelessWidget{

  String title = "Perfil";

  GradientBackProfile(this.title);
  @override
  Widget build(BuildContext context) {

    return Container(
      height: 400,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                Color(0XFF4268D3),
                Color(0XFF584CD1)
              ],
              begin: FractionalOffset(0.2, 0.0),
              end: FractionalOffset(1.0, 0.6),
              stops: [0.0, 0.6],
              tileMode: TileMode.clamp
          )
      ),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Lato",
                fontSize: 30.0
            ),
          ),
          FloatingActionButton(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Icon(
              Icons.settings,
              size: 15,
            ),
          ),
        ],
      ),
      alignment: Alignment(-0.75, -0.75),
    );
  }
}