import 'package:flutter/material.dart';

import 'nav_floating_buttons_profile.dart';
import 'content_view_profile.dart';

class Profile extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        ContentViewProfile(),
        NavFloatingButtonsProfile(),
      ],
    );
  }
}