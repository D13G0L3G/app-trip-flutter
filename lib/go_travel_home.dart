import 'package:flutter/material.dart';

import 'ViewTrips/home_trips.dart';
import 'ViewProfile/profile.dart';
import 'ViewSearch/search_trips.dart';


class GoTravelHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GoTravelHome();
  }
}

class _GoTravelHome extends State<GoTravelHome>{

  int indexTap = 0;

  final List<Widget> widgetsChildren = [
    HomeTrips(),
    SearchTrips(),
    Profile(),
  ];

  void onTapTapped(int index){
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.white,
          primaryColor: Colors.purple
        ),
        child: BottomNavigationBar(
          onTap: onTapTapped,
          currentIndex: indexTap,
            items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: ""
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: ""
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: ""
          )
        ])
      ),
    );
  }

}