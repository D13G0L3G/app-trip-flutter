import 'package:flutter/material.dart';

import 'gradient_back_home_trips.dart';
import 'card_image_list.dart';

class HeaderAppBar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GradientBack("Go Travel"),
        CardImageList()
      ],
    );
  }
}