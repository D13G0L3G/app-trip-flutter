import 'package:flutter/material.dart';

import 'description_place.dart';
import 'header_appbar.dart';
import 'review_list.dart';

class HomeTrips extends StatelessWidget{

  String descriptionDummy = '''Ciudad perdida - Cuidad Sagrada - Cuna del Imperio Inca, Machupicchu Ubicado a 2.430 metros de altura en un paraje de gran belleza, en medio de un bosque tropical de montaña, el santuario de Machu Picchu fue probablemente la realización arquitectónica más asombrosa del Imperio Inca en su apogeo.
          
          Sus murallas, terrazas y rampas gigantescas dan la impresión de haber sido esculpidas en las escarpaduras de la roca, como si formaran parte de ésta.''';

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          children: [
            DescriptionPlace("Machu Picchu", descriptionDummy),
            ReviewList(),
          ],
        ),
        HeaderAppBar(),
      ],
    );
  }
}